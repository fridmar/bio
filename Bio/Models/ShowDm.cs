﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class ShowDm
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        [DisplayName("Tid")]
        public System.DateTime StartDate { get; set; }
        [DisplayName("Platser kvar")]
        public int SeatsAvailable { get; set; }


        public MoviesDm Movy { get; set; }
        public ICollection<SeatDm> Seats { get; set; }
        public ICollection<TicketDm> Tickets { get; set; }
    }
}


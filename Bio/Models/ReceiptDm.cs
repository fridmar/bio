﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class ReceiptDm
    {
        public string Date { get; set; }
        public string MovieName { get; set; }
        public int TotalAdults { get; set; }
        public int TotalChildren { get; set; }
        public string Email { get; set; }
        public int TotalPrice { get; set; }
        public List<ReceiptTicket> Tickets { get; set; }
    }
}
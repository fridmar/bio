﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class TicketDm
    {
        public int Id { get; set; }
        public Nullable<int> Price { get; set; }
        public int ShowId { get; set; }
        public int SeatsId { get; set; }
        public int BookingId { get; set; }

        public BookingsDm Booking { get; set; }
        public SeatDm Seat { get; set; }
        public ShowDm Show { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bio.Models
{
    public class BookingsDm
    {
        public int Id { get; set; }
        
        public DateTime Date { get; set; }

        [Display(Name = "Ange e-postadress :")]
        [EmailAddress]
        public string Email { get; set; }

        public int CinemaId { get; set; }

        [Display(Name = "Jag godkänner Cinema palace köpvillkor")]
        public bool AcceptConditions { get; set; }
        public virtual ICollection<TicketDm> Tickets { get; set; }
    }
}
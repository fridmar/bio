﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class SeatDm
    {
        public int Id { get; set; }
        public string Row { get; set; }
        public int SeatNr { get; set; }
        public int ShowId { get; set; }

        public ShowDm Show { get; set; }
        public ICollection<TicketDm> Tickets { get; set; }
    }
}
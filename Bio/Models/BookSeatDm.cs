﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class BookSeatDm
    {
        public List<string> Rows { get; set; }
        public ShowDm Show { get; set; }
        public int TotalSeatsPerRow { get; set; }
    }
}
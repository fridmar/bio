﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class RowDm
    {
        public string RowName { get; set; }
        public List<SeatDm> Seats { get; set; }
    }
}
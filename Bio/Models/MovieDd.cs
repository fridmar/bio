﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bio.Models
{
    public class MovieDd
    {
        public IEnumerable<SelectListItem> Movies { get; set; }
        public int MovieId { get; set; }
    }
}
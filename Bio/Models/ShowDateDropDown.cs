﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bio.Models
{
    public class ShowDateDropDown
    {
        public IEnumerable<SelectListItem> Shows { get; set; }
        public int ShowId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Models
{
    public class ReceiptSeat
    {
        public Nullable<int> Price { get; set; }
        public string MovieName { get; set; }
        public int SeatNr { get; set; }
        public string RowNr { get; set; }
        public int Time { get; set; }        
    }
}
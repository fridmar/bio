﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.DAL;
using Bio.Models;
using System.Data.Entity.Validation;

namespace Bio.Repository
{
    public class Repo : IRepo
    {
        private CinemaApplicationEntities context = new CinemaApplicationEntities();

        public void CreateBooking(Booking booking)
        {
            context.Bookings.Add(booking);
            context.SaveChanges();
        }

        public void MakeReservation(Booking reservation)
        {
            context.Bookings.Add(reservation);
            context.SaveChanges();
        }

        public List<Models.MoviesDm> GetAllMovies()
        {
            return context.Movies.Select(m => new Models.MoviesDm { Id = m.Id, Name = m.Name }).OrderBy(m =>m.Name).ToList();
        }


        public List<Models.CinemasDm> GetAllCinemas()
        {
            return context.Cinemas.Select(c => new Models.CinemasDm { Id = c.Id, Name = c.Name }).ToList();
        }


        public List<Models.ShowDm> GetSelectedMovieShows(int id)
        {
            return context.Shows.Where(s => s.MovieId == id)
                .Select(ms => new ShowDm
                {
                    Id = ms.Id,
                    StartDate = ms.StartDate,
                }).ToList();
        }


        public List<Models.ShowDm> GetSelectedMovieShowDates(int id)
        {
            var list = context.Shows.Where(s => s.MovieId == id)
               .Select(ms => new ShowDm
               {
                   Id = ms.Id,
                   StartDate = ms.StartDate,
               }).ToList();

            var showsWithoutDuplicateDates = new List<ShowDm>();

            showsWithoutDuplicateDates = list.GroupBy(s => s.StartDate.Date)
                .Select(g => g.First()).ToList();

            return showsWithoutDuplicateDates;
        }


        public BookSeatDm CreateBookSeatDm(int showId)
        {
            ShowDm show = context.Shows.Where(s => s.Id == showId).Select(s => new ShowDm
            {
                Id = s.Id,
                Seats = s.Seats.Select(seat => new SeatDm
                {
                    Id = seat.Id,
                    SeatNr = seat.SeatNumber,
                }).ToList(),

            }).FirstOrDefault();

            BookSeatDm model = new BookSeatDm
            {
                Rows = new List<string> { "A", "B", "C", "D", "E" },
                Show = show,
                TotalSeatsPerRow = 10,
            };
            return model;
        }

        public int CreateBooking(List<SeatBookListDm> bookedSeats, int showId)
        {
            List<Ticket> tickets = new List<Ticket>();

            foreach (var item in bookedSeats)
            {
                tickets.Add(new Ticket
                {
                    SeatId = context.Seats.FirstOrDefault(s => s.SeatNumber == item.SeatNr).Id,
                    ShowId = showId,
                    Price = 98,                    
                });
            }

            Booking booking = new Booking
            {
                Date = DateTime.Now,
                Tickets = tickets,
                Email = "",
                CinemaId = 1,
            };

            context.Bookings.Add(booking);
            context.SaveChanges();

            return booking.Id;
        }

        public Models.MoviesDm GetMovie(int id)
        {
            return context.Movies.Where(m => m.Id == id).Select(m => new Models.MoviesDm { Id = m.Id, Name = m.Name }).FirstOrDefault();
        }

        //public Models.BookingsDm GetBooking(int id)
        //{

        //    return context.Bookings.Where(b => b.Id == id).Select(b => new Models.BookingsDm { Id = b.Id, Email = b.Email, Date = b.Date, CinemaId = b.CinemaId, Tickets = b.Tickets }).FirstOrDefault();
        //}

        public Models.ShowDm GetShow(int id)
        {
            return context.Shows.Where(s => s.Id == id).Select(s => new Models.ShowDm { Id = s.Id, MovieId = s.MovieId, StartDate = s.StartDate }).FirstOrDefault();
        }



        public List<ShowDm> GetSelectedMovieShowDateTimes(int id)
        {
            var show = context.Shows.FirstOrDefault(s => s.Id == id);

            DateTime showsFrom = show.StartDate.Date;
            DateTime showsTo = showsFrom.AddDays(1);

            var showsOnSelectedDate = context.Shows.Where(s => s.MovieId == show.MovieId && s.StartDate >= showsFrom && s.StartDate <= showsTo)
                .Select(sh => new ShowDm
                {
                    Id = sh.Id,
                    StartDate = sh.StartDate,
                    SeatsAvailable = 50 - sh.Seats.Count(),
                }).ToList();

            return showsOnSelectedDate;
        }



        public ShowDm GetShowById(int id)
        {
            return context.Shows.Where(s => s.Id == id)
                .Select(sh => new ShowDm
                {
                    Id = sh.Id,
                }).FirstOrDefault();
        }


        public bool BookSeats(List<SeatBookListDm> bookedSeatsIn, int showId)
        {
            Show show = context.Shows.Find(showId);

            if (bookedSeatsIn != null && show != null)
            {
                foreach (var item in bookedSeatsIn)
                {
                    Seat seat = context.Seats.Find(item.SeatNr);
                    show.Seats.Add(seat);

                    context.SaveChanges();
                }
                return true;
            }
            else
                return false;
        }

        public List<Models.ReceiptTicket> GetTickets(int id)

        {
            var tickets = context.Tickets.Where(t => t.BookingId == id).Select(t => new Models.ReceiptTicket { Price = t.Price, RowNr = t.Seat.Row.RowName, SeatNr = t.Seat.SeatNumber,  ShowId = t.ShowId, TicketId = (t.ShowId + t.Price + t.Seat.SeatNumber).ToString() }).ToList();          
            return  tickets;
        }

        public Models.BookingsDm GetBooking(int id)
         {                                 
            return context.Bookings.Where(b => b.Id == id).Select(b => new Models.BookingsDm { Id = b.Id, Email = b.Email, Date = b.Date, CinemaId = b.CinemaId}).FirstOrDefault();
         }


        public string GetMovieByBookingId(int id)
        {
            var ticket = context.Bookings.Find(id).Tickets.FirstOrDefault(t => t.Id == 1);
            var show = context.Shows.Find(ticket.ShowId);
            return show.Movie.Name;
            

            //var showid = context.Bookings.Where(b => b.Id == id).FirstOrDefault().Tickets.FirstOrDefault().ShowId;

            //var movieName = context.Shows.Where(s => s.Id == showid).FirstOrDefault().Movie.Name;
            //return movieName;
        }

        public Models.ReceiptDm GetReceiptByBookingId(int bookingId)
        {

            List<ReceiptTicket> tickets = GetTickets(bookingId);
            string email = context.Bookings.Find(bookingId).Email;
            //var selectedMovie = GetMovieByBookingId(bookingId);
            var ticket = tickets[0];
            var show = GetShow(ticket.ShowId);

            string ticketPrice = tickets[1].Price.ToString();
            int intticketPrice = int.Parse(ticketPrice);
            ReceiptDm newReceipt = new ReceiptDm
            {
                Date = (show.StartDate.ToString("yyyy d MMMM") + " Kl: " + show.StartDate.ToString("HH mm")),
               // MovieName = (selectedMovie),
                TotalAdults = (0),
                TotalChildren = 1,
                Email = (email + "<- ditt mejl"),
                Tickets = tickets,
                TotalPrice = intticketPrice * tickets.Count(),
            };

            return newReceipt;
        }

    }
}
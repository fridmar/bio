﻿using Bio.DAL;
using Bio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Repository
{
    interface IRepo 
    {
        List<MoviesDm> GetAllMovies();
        List<CinemasDm> GetAllCinemas();
        List<ShowDm> GetSelectedMovieShows(int id);
        List<ShowDm> GetSelectedMovieShowDates(int id);
        List<ShowDm> GetSelectedMovieShowDateTimes(int id);
        BookSeatDm CreateBookSeatDm(int showId);
        int CreateBooking(List<SeatBookListDm> bookedSeats, int showId);
       // List<ShowDm> GetShowStartTitmeOnSelectedDate(int id);
        bool BookSeats(List<SeatBookListDm> bookedSeatsIn, int showId);
        BookingsDm GetBooking(int id);
        ShowDm GetShowById(int id);
    }
}

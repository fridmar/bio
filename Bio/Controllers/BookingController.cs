﻿using Bio.DAL;
using Bio.Models;
using Bio.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bio.Controllers
{
    public class BookingController : Controller
    {
        public ActionResult Index()
        {
            IRepo repo = new Repo();
            var model = repo.GetAllMovies();

            return View(model);
        }

        [HttpGet]
        public ActionResult ShowDates(int id)
        {
            IRepo repo = new Repo();
            var shows = repo.GetSelectedMovieShowDates(id);

            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in shows)
            {
                var a = new SelectListItem() { Text = item.StartDate.ToShortDateString(), Value = item.Id.ToString() };
                list.Add(a);
            }
            ShowDateDropDown model = new ShowDateDropDown()
            {
                Shows = new SelectList(list, "Value", "Text")
            };

            return PartialView("_ShowDates", model);
        }


        [HttpGet]
        public ActionResult ShowDateTimes(int id)
        {
            IRepo repo = new Repo();
            var model = repo.GetSelectedMovieShowDateTimes(id);
            return PartialView("_ShowDateTimes", model);
        }

        [HttpGet]
        public ActionResult TestOne(int id)
        {
            IRepo repo = new Repo();
            var model = repo.GetShowById(id);
            return RedirectToAction("Seat", "Booking", new { showId = id });
        }




        [HttpPost]
        public ActionResult Seat(List<SeatBookListDm> bookedSeats, int showId)
        {
            IRepo repo = new Repo();
            bool success = repo.BookSeats(bookedSeats, showId);
            if (success)
            {
                int bookingId = repo.CreateBooking(bookedSeats, showId);
                return Json(new { success = success, redirectTo = Url.Action("BuyOrReserveTickets", "Booking", new { bookingIdIn = bookingId }) });
            }
            else
                return Json(new { success = success, responseText = "Du måste boka minst ett säte!" });
        }




        public ActionResult Seat(int showId)
        {
            IRepo repo = new Repo();
            BookSeatDm model = repo.CreateBookSeatDm(showId);

            return View(model);
        }


        public ActionResult Payment()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BuyOrReserveTickets(int bookingIdIn)
        {
            IRepo repo = new Repo();
            var x = repo.GetBooking(bookingIdIn);
            return View(x);
        }


        [HttpPost]
        public ActionResult Receipt(BookingsDm model)
        {
            //int bookingId = 3;
            Repo rep = new Repo();
            ReceiptDm newReceipt = rep.GetReceiptByBookingId(model.Id);
            return View(newReceipt);
        }
    }
}
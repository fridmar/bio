﻿var totalSelectedSeats = 0;
var totaltickets = 2;
var selectedSeats = [];
var totalSum = 0;

$(document).ready(function () {

    var showIdIn = $('#showId').val();


    document.getElementById("totalSum").innerHTML = "<b>" + totalSum + " kr</b>";

    $('#bookSeatsBtn').on('click', function (e) {
        e.preventDefault();

        if (selectedSeats.length > 0) {
            var idList = [];

            $('#choosenseats').children().children('td').each(function () {
                idList.push($(this).attr("id"));
            });

            var seatObjectList = [];

            for (var i = 0; i < idList.length; i++) {
                var seatSplit = idList[i].split('-');
                seatObjectList.push({ Row: seatSplit[0], SeatNr: parseInt(seatSplit[1]) })
            }

            $.ajax({
                url: '/Booking/Seat',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({ bookedSeats: seatObjectList, showId: showIdIn }),
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    if (result.success) {
                        window.location.href = result.redirectTo;
                    }
                    else {
                        document.getElementById("booked-or-not").innerHTML = result.responseText;
                    }
                }
            });
        }
        else {
            document.getElementById("booked-or-not").innerHTML = "Du måste boka minst ett säte!";
        }
    });
});




function seatClick(imgIn, idIn) {
    console.log(imgIn);
    console.log(idIn);
    var fileNameIndex = imgIn.src.lastIndexOf("/") + 1;
    var filename = imgIn.src.substr(fileNameIndex);

    if (totalSelectedSeats < 6 && filename != "RedChair.png") {
        if (filename == "GreenChair.png") {
            imgIn.src = "../Images/WhiteChair.png";
        } else {
            imgIn.src = "../Images/GreenChair.png";
        }
        totalSelectedSeatsChange(filename, idIn);
        document.getElementById("booked-or-not").innerHTML = "";
    }
    else if (totalSelectedSeats == 6 && filename == "GreenChair.png") {
        totalSelectedSeatsChange(filename, idIn);
        imgIn.src = "../Images/WhiteChair.png";
        document.getElementById("booked-or-not").innerHTML = "";
    }
    else {
        document.getElementById("booked-or-not").innerHTML = "Max antal platser eller redan bokat säte!";
    }
}

function totalSelectedSeatsChange(filenameIn, idIn) {

    if (filenameIn == "WhiteChair.png") {
        totalSelectedSeats++;
        selectedSeats.push("<tr><td id='" + idIn + "' >" + idIn + "</td></tr>")
        totalSum += 98;
    }
    else if (filenameIn == "GreenChair.png") {
        totalSelectedSeats--;
        var index = selectedSeats.indexOf("<tr><td id='" + idIn + "' >" + idIn + "</td></tr>");
        selectedSeats.splice(index, 1);
        totalSum -= 98;
    }
    selectedSeats.sort();
    document.getElementById("choosenseats").innerHTML = selectedSeats.join('');
    document.getElementById("totalSum").innerHTML = "<b>" + totalSum + " kr</b>";
}




function totalticketChange() {

    totaltickets = parseInt(totalAdults.value) + parseInt(totalChilds.value);
}

function increaseAdult() {
    console.log("increased");
    var currentValue = document.getElementById("totalAdults").value;
    totalticketChange();
    if (totaltickets < 6) {
        var newValue = parseInt(currentValue) + 1;
        document.getElementById("totalAdults").value = newValue;
    }
}


function decreaseAdult() {
    console.log("decreaseAdult");
    var currentValue = document.getElementById("totalAdults").value;
    totalticketChange();
    if (totaltickets > 0) {
        var newValue = parseInt(currentValue) - 1;
        document.getElementById("totalAdults").value = newValue;

    }
}

function increaseChild() {
    console.log("increaseChild");
    var currentValue = document.getElementById("totalChilds").value;
    totalticketChange();
    if (totaltickets < 6) {
        var newValue = parseInt(currentValue) + 1;
        document.getElementById("totalChilds").value = newValue;

    }
}

function decreaseChild() {
    console.log("decreaseChild");
    var currentValue = document.getElementById("totalChilds").value;
    totalticketChange();
    if (totaltickets > 0) {
        var newValue = parseInt(currentValue) - 1;
        document.getElementById("totalChilds").value = newValue;

    }
}